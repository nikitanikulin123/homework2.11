'use strict';

angular
    .module('myApp')
    .controller('PrivateCabinetCtrl', function() {

    var vm = this;
    vm.newAccount = {};
    vm.addAccount = function(myAccount) {
        console.log(myAccount);
        vm.newAccount = {};
        vm.accountForm.$setPristine();
    };

});
